/**+--------------------------------------------------------------------------------+
 * |                                      Right                                     |
 * +--------------------------------------------------------------------------------+
 */
class Right {

    /**
     * Constructor of the 'Right' class.

     * @param {String} value Value of the object in the expected key. 
     */

    constructor(value) {
        this.value = value;
    }

    /**
     * Returns the 'Right.value' string but modified.
     * 
     * @params  {funtion(String)}   fn  In this case, this function must returns a String.
     * @returns {String}            --  String that shows the result of the passed function (fn).
     */

    map = fn => `Right("${fn(this.value)}")`

    /**
     * Will run a function or another depending if the consulted
     * object has a property with an specific key.
     * 
     * For the fact that this is a 'Left' object, the object did have
     * the key.
     * 
     * @params  {function(String)} errorFn   Will never be called.
     * @params  {function(String)} fn        Will always be called.
     * @returns {String}           --        Result of the passed function.
     */

    runEither = (errorFn, fn) => fn(this.value)

    /**
     * Indicates the type, this is a 'sub-object' of 'Either'
     * 
     * @returns {String} -- Type of the object
     */

    getType = () => 'Either'
}
/**+--------------------------------------------------------------------------------+
 * |                                      Left                                      |
 * +--------------------------------------------------------------------------------+
 */
class Left {

    /**
     * Constructor of the 'Left' object.
     * 
     * @param {String} value Value of the object in the expected key. 
     */

    constructor(value) {
        this.value = value;
    }

    /**
     * Returns the 'Left.value' string but modified.
     * 
     * @params  {funtion(String)}   fn  In this case, this function must returns a String.
     * @returns {String}            --  String that shows the result of the passed function (fn).
     */

    map = (fn) => `Left("${fn(this.value)}")`

    /**
     * Will run a function or another depending if the consulted
     * object has a property with an specific key.
     * 
     * For the fact that this is a 'Left' object, the object did not
     * have the key.
     * 
     * @params  {function(String)} errorFn   Will always be called.
     * @params  {function(String)} fn        Will never be called.
     * @returns {String}           --        Result of the passed function.
     */

    runEither = (errorFn, fn) => errorFn(this.value)

    /**
     * Indicates the type, this is a 'sub-object' of 'Either'
     * 
     * @returns {String} -- Type of the object
     */

    getType = () => 'Either'
}

/**+--------------------------------------------------------------------------------+
 * |                                     Either                                     |
 * +--------------------------------------------------------------------------------+
 */
module.exports = class Either {

    /**
     * @params  {String} value Value of the consulted object's property.
     * @returns {Right} --    Right object.
     */

    createRight = value => new Right(value)

    /**
     * @params  {String} key Key of the consulted object's property.
     * @returns {Right} --  Left object.
     */

    createLeft = key => new Left(key)
}