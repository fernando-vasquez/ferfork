const Either = require("./index.js");
const either = new Either();

const prop = (key, object) =>
  key in object
    ? either.createRight(object[key])
    : either.createLeft(`Cannot read property '${key}'`);

const foobar = prop("foo", { foo: "bar" });
const bazbar = prop("baz", { foo: "bar" });

console.log(foobar.map(word => `${word}!`)); // -> Right("bar!")
console.log(bazbar.map(word => `${word}!`)); // -> Left("Cannot read property 'baz'")
foobar.runEither(error => console.error(new Error(error)), console.log); // -> foo
bazbar.runEither(error => console.error(new Error(error)), console.log); // -> Error: "Cannot read property 'baz'" at ...
console.log(prop("foo", { foo: "bar" }).getType()); // -> Either
console.log(prop("baz", { foo: "bar" }).getType()); // -> Either