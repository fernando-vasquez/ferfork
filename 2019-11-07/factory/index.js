/**+--------------------------------------------------------------------------------+
 * |                                      Right                                     |
 * +--------------------------------------------------------------------------------+
 */

/**
 * 'Right' object.
 * 
 * @param {String}   value Value of the object in the expected key. 
 * @returns {Object} --    Object with methods to be used.
 */

const Right = value => {
    return {

        /**
         * Returns the 'Right.value' string but modified.
         * 
         * @params  {funtion(String)}   fn  In this case, this function must returns a String.
         * @returns {String}            --  String that shows the result of the passed function (fn).
         */

        map: fn => `Right("${fn(value)}")`,

        /**
         * Will run a function or another depending if the consulted
         * object has a property with an specific key.
         * 
         * For the fact that this is a 'Left' object, the object did have
         * the key.
         * 
         * @params  {function(String)} errorFn   Will never be called.
         * @params  {function(String)} fn        Will always be called.
         * @returns {String}           --        Result of the passed function.
         */

        runEither: (errorFn, fn) => fn(value),

        /**
         * Indicates the type, this is a 'sub-object' of 'Either'
         * 
         * @returns {String} -- Type of the object
         */

        getType: () => 'Either'
    }
}

/**+--------------------------------------------------------------------------------+
 * |                                      Left                                      |
 * +--------------------------------------------------------------------------------+
 */
/**
 * 'Left' object.
 * 
 * @param {String}   value Value of the object in the expected key. 
 * @returns {Object} --    Object with methods to be used.
 */

const Left = (value) => {
    return {

        /**
         * Returns the 'Left.value' string but modified.
         * 
         * @params  {funtion(String)}   fn  In this case, this function must returns a String.
         * @returns {String}            --  String that shows the result of the passed function (fn).
         */

        map: fn => `Left("${fn(value)}")`,

        /**
         * Will run a function or another depending if the consulted
         * object has a property with an specific key.
         * 
         * For the fact that this is a 'Left' object, the object did not
         * have the key.
         * 
         * @params  {function(String)} errorFn   Will always be called.
         * @params  {function(String)} fn        Will never be called.
         * @returns {String}           --        Result of the passed function.
         */

        runEither: (errorFn, fn) => errorFn(value),

        /**
         * Indicates the type, this is a 'sub-object' of 'Either'
         * 
         * @returns {String} -- Type of the object
         */

        getType: () => 'Either'
    }
}

/**+--------------------------------------------------------------------------------+
 * |                                     Either                                     |
 * +--------------------------------------------------------------------------------+
 */
/**
 * 'Either' object
 */
const Either = {

    /**
     * @params {String} value Value of the consulted object's property.
     * @returns {Right} --    Right object.
     */
    createRight: value => Right(value),

    /**
     * @params {String} key Key of the consulted object's property.
     * @returns {Right} --  Left object.
     */
    createLeft: key => Left(key),
}

module.exports = Either;