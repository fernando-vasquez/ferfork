/**+--------------------------------------------------------------------------------+
 * |                                      Right                                     |
 * +--------------------------------------------------------------------------------+
 */

/**
 * Constructor of the 'Right' object.
 * 'this' is bind for the 'map' and 'runEither' methods.
 * 
 * @param {String} value Value of the object in the expected key. 
 */

function Right(value) {
    this.value = value;
    this.map = this.map.bind(this);
    this.runEither = this.runEither.bind(this);
}

/**
 * Returns the 'Right.value' string but modified.
 * 
 * @params  {funtion(String)}   fn  In this case, this function must returns a String.
 * @returns {String}            --  String that shows the result of the passed function (fn).
 */

Right.prototype.map = function (fn) {
    return `Right("${fn(this.value)}")`
}

/**
 * Will run a function or another depending if the consulted
 * object has a property with an specific key.
 * 
 * For the fact that this is a 'Left' object, the object did have
 * the key.
 * 
 * @params  {function(String)} errorFn   Will never be called.
 * @params  {function(String)} fn        Will always be called.
 * @returns {String}           --        Result of the passed function.
 */

Right.prototype.runEither = function (errorFn, fn) {
    return fn(this.value);
}

/**
 * Indicates the type, this is a 'sub-object' of 'Either'
 * 
 * @returns {String} -- Type of the object
 */

Right.prototype.getType = () => 'Either';


/**+--------------------------------------------------------------------------------+
 * |                                      Left                                      |
 * +--------------------------------------------------------------------------------+
 */
/**
 * Constructor of the 'Left' object.
 * 'this' is bind for the 'map' and 'runEither' methods.
 * 
 * @param {String} value Value of the object in the expected key. 
 */

function Left(value) {
    this.value = value;
    this.map = this.map.bind(this);
    this.runEither = this.runEither.bind(this);
}

/**
 * Returns the 'Left.value' string but modified.
 * 
 * @params  {funtion(String)}   fn  In this case, this function must returns a String.
 * @returns {String}            --  String that shows the result of the passed function (fn).
 */

Left.prototype.map = function (fn) {
    return `Left("${fn(this.value)}")`;
}

/**
 * Will run a function or another depending if the consulted
 * object has a property with an specific key.
 * 
 * For the fact that this is a 'Left' object, the object did not
 * have the key.
 * 
 * @params  {function(String)} errorFn   Will always be called.
 * @params  {function(String)} fn        Will never be called.
 * @returns {String}           --        Result of the passed function.
 */

Left.prototype.runEither = function (errorFn, fn) {
    return errorFn(this.value);
}

/**
 * Indicates the type, this is a 'sub-object' of 'Either'
 * 
 * @returns {String} -- Type of the object
 */

Left.prototype.getType = () => 'Either';


/**+--------------------------------------------------------------------------------+
 * |                                     Either                                     |
 * +--------------------------------------------------------------------------------+
 */

/**
 * Constructor of the 'Either' object
 */

function Either() { }

/**
 * @params  {String} value Value of the consulted object's property.
 * @returns {Right} --    Right object.
 */

Either.prototype.createRight = value => new Right(value);

/**
 * @params  {String} key Key of the consulted object's property.
 * @returns {Right} --  Left object.
 */

Either.prototype.createLeft = key => new Left(key);

module.exports = Either;